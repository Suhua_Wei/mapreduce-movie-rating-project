import os
from math import sqrt
from itertools import combinations
import multiprocessing 
import cProfile

class MovieSimilarities():
    def __init__(self):
        # self.movieName = raw_input("Type the movie name file , for exmaple u.item\n")
        # self.ratingData = raw_input("Type the movie rating file, for example u.data \n")
        self.movieName = 'u.item'
        self.ratingData ='u.data'
        self.intermediatePrefix = 'intermediate'
        self.cpuCount = multiprocessing.cpu_count() - 1
        self.movieNames = {}


    def load_movie_names(self):
        # Load database of movie names.
        with open(self.movieName) as f:
            for line in f:
                fields = line.split('|')
                if (fields[0] != 'movieId'): # Skip first line
                    self.movieNames[int(fields[0])] = fields[1].decode('utf-8', 'ignore')

    def parse_input(self):
        # Outputs userID => (movieID, rating)

        users = {}
        with open(self.ratingData, 'rb') as f:
            for line in f.readlines():
                
                (userID, movieID, rating, timestamp) = line.split()
                if (userID != 'userId'): #skip the first line
                    itemRating = (movieID, rating)
                    if userID in users:
                        users[userID].append(itemRating)
                    else:
                        users[userID]=list()
                        users[userID].append(itemRating)

        return users

    def write_movie_rating_file(self, filename, users):
        with open(filename, 'w+') as f:
            for userID in users:

                # movie_pair, rating, reverse_pair, reverse_rating = self.mapper_create_item_pairs(userID, users[userID])
                item_pairs = self.mapper_create_item_pairs(userID, users[userID])
                for moviePair in item_pairs:
                    i1=",".join([str(moviePair[0][0]),str(moviePair[0][1])])+'\t'+",".join([str(moviePair[1][0]),str(moviePair[1][1])])
                    i2=",".join([str(moviePair[2][0]),str(moviePair[2][1])])+'\t'+",".join([str(moviePair[3][0]),str(moviePair[3][1])])
                    f.write(i1+'\n'+i2+'\n')

    def mapper_create_item_pairs(self, user_id, ratings):
        # Find every pair of movies each user has seen, and emit
        # each pair with its associated ratings

        # "combinations" finds every possible pair from the list of movies
        # this user viewed.
 
        item_pairs = list()
        for itemRating1, itemRating2 in combinations(ratings, 2):
            movieID1 = itemRating1[0]
            rating1 = itemRating1[1]
            movieID2 = itemRating2[0]
            rating2 = itemRating2[1]
            # Produce both orders so sims are bi-directional
            item_pairs.append(((movieID1,movieID2), (rating1,rating2), (movieID2,movieID1), (rating2,rating1)))

        return item_pairs


    def cosine_similarity(self, ratingPairs):
        # Computes the cosine similarity metric between two
        # rating vectors.
        numPairs = 0
        sum_xx = sum_yy = sum_xy = 0
        for ratingPair in ratingPairs:
            ratingX, ratingY = ratingPair.split(',')
            sum_xx += float(ratingX) * float(ratingX)
            sum_yy += float(ratingY) * float(ratingY)
            sum_xy += float(ratingX) * float(ratingY)
            numPairs += 1

        numerator = sum_xy
        denominator = sqrt(sum_xx) * sqrt(sum_yy)


        score = 0
        if (denominator):
            score = (numerator / (float(denominator)))

        return (score, numPairs)

    def reducer_compute_similarity(self, moviePair, ratingPairs):
        # Compute the similarity score between the ratings vectors
        # for each movie pair viewed by multiple people

        # Output movie pair => score, number of co-ratings

        score, numPairs = self.cosine_similarity(ratingPairs)
        # Enforce a minimum score and minimum number of co-ratings
        # to ensure quality
        if (numPairs > 10 and score > 0.95):
            return moviePair, (score, numPairs)
        else:
            return None

    def create_movie_pair_ratings(self, inputfile):
        moviePairs = {}
        with open(inputfile, 'rb') as f:
            for line in f.readlines():
                moviePair, ratingPair = line.split()
                if moviePair in moviePairs:
                    moviePairs[moviePair].append(ratingPair)
                else:
                    moviePairs[moviePair]=list()
                    moviePairs[moviePair].append(ratingPair)
        return moviePairs
    def create_move_sims(self, moviePairs):
        movie_sims = {}  
        for moviePair in moviePairs:
            movieScore = self.reducer_compute_similarity(moviePair, moviePairs[moviePair])
            # skip to next iteration if result is none
            if not movieScore:

                continue
      
            movie1, movie2= movieScore[0].split(',')
            score, n =  movieScore[1]



            if movie1 in movie_sims:
                movie_sims[movie1].append((movie2,(score, n)))
            else:
                movie_sims[movie1]=list()
                movie_sims[movie1].append((movie2,(score, n)))
        return movie_sims

    def write_to_output(self,sims_file, movie_sims):
        with open(sims_file, 'w+') as f:
            for movie in movie_sims:
                # sort the similarity score
                value = movie_sims[movie]
              
                topN = sorted(value, key=lambda x : x[1][0],reverse=True)
                # f.write(self.movieNames[int(movie)] + '::')
                for i in range(len(topN)):
                    try:
                        sim = topN[i]
                        f.write(self.movieNames[int(movie)] + '\t\t')
                        f.write(self.movieNames[int(sim[0])]+ ',' + str(sim[1][0])+ ','+str(sim[1][1])+'\n')

                    except Exception as e:
                        print str(e)
                        pass
        
    def run(self):
        self.load_movie_names()
        users = self.parse_input()
        movie_rating_file = 'movie_rating.txt'
        self.write_movie_rating_file(movie_rating_file, users)
        moviePairs=self.create_movie_pair_ratings(movie_rating_file)
        movie_sims=self.create_move_sims(moviePairs)
        self.write_to_output('sims.txt', movie_sims)
       


if __name__ == '__main__':
    m=MovieSimilarities()
    cProfile.run('m.run()')
