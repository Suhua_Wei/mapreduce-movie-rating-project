import os
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
from pyspark import SparkContext
import math 
from time import time
import cProfile
def get_counts_and_averages(ID_and_ratings_tuple):
    nratings = len(ID_and_ratings_tuple[1])
    return ID_and_ratings_tuple[0], (nratings, float(sum(float(x) for x in ID_and_ratings_tuple[1]))/nratings)

def main():
    
    data_path = '/home/sue/Downloads/ml-latest-small'
    data_file = os.path.join(data_path, 'ratings.csv')
    sc = sc = SparkContext(appName="MoviesExample")
    rating_raw_data = sc.textFile(data_file)
    rating_raw_data_header = rating_raw_data.take(1)[0]


    #parse the raw data into new RDD
    rating = rating_raw_data.filter(lambda line: line!=rating_raw_data_header).map(lambda line:line.split(",")).map(lambda fields:(fields[0], fields[1], fields[2])).cache()

    #display sample data
    # print rating.take(3)

    movie_file = os.path.join(data_path, 'movies.csv')
    movie_raw_data = sc.textFile(movie_file)
    movie_raw_data_header = movie_raw_data.take(1)[0]
    movies = movie_raw_data.filter(lambda line: line!=movie_raw_data_header).map(lambda line:line.split(",")).map(lambda fields: (fields[0], fields[1])).cache()

    # print movies.take(3)


    #split datasets into training, validation and test

    training_rdd, validation_rdd, test_rdd = rating.randomSplit([6,2,2], seed=0L)
    validation_for_predict_rdd = validation_rdd.map(lambda x:(x[0], x[1]))
    test_for_predict_rdd = test_rdd.map(lambda x:(x[0], x[1]))


    #training phrase
    seed = 5L
    iterations = 10
    regularization_parameter = 0.1
    ranks = [4, 8, 12]
    errors = [0,0,0]
    err = 0
    tolerance = 0.02

    min_error = float('inf')
    for rank in ranks:
        model = ALS.train(training_rdd, rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)
        predictions = model.predictAll(validation_for_predict_rdd).map(lambda r: ((r[0], r[1]), r[2]))
        rates_and_preds = validation_rdd.map(lambda r:((int(r[0]), int(r[1])),float(r[2]))).join(predictions)
        error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1])**2).mean())
        errors[err] = error
        err += 1
        print 'For rank %s the RMSE is %s' % (rank, error)
        if error < min_error:
            min_error = error
            best_rank = rank
    print 'The best model was trained with rank %s' % best_rank


    #select the best rank model
    model = ALS.train(training_rdd, best_rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)
    predictions = model.predictAll(test_for_predict_rdd).map(lambda r: ((r[0], r[1]), r[2]))
    rates_and_preds = test_rdd.map(lambda r:((int(r[0]), int(r[1])),float(r[2]))).join(predictions)
    error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1])**2).mean())

    #run the model at a larger dataset
    # complete_set = os.path.join('/home/sue/Downloads/ml-latest', 'ratings.csv')
    # complete_raw_data = sc.textFile(complete_set)
    # complete_rating_header = complete_raw_data.take(1)[0]
    # complete_rating = complete_raw_data.filter(lambda line: line!=complete_rating_header).map(lambda line:line.split(",")).map(lambda fields:(fields[0], fields[1], fields[2])).cache()
    
    # print "There are %s recommendations in the complete dataset" % (complete_rating.count())


    # #split into training and testing
    # training_rdd, test_rdd = complete_rating.randomSplit([7,3], seed=0L)
    # complete_model = ALS.train(training_rdd, best_rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)


    # #test model
    # test_for_predict_rdd = test_rdd.map(lambda x:(x[0],x[1]))
    # predictions = complete_model.predictAll(test_for_predict_rdd).map(lambda r:((r[0],r[1]),r[2]))
    # rates_and_preds = test_rdd.map(lambda r:((int(r[0]), int(r[1])), float(r[2]))).join(predictions)
    # error =  math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1])**2).mean())
    # print 'For the testing data the RMSE is %s' % (error)

    # #load movie titles
    # complete_moive_file = os.path.join('/home/sue/Downloads/ml-latest', 'movies.csv')
    # complete_movie_data = sc.textFile(complete_moive_file)
    # complete_movie_header = complete_movie_data.take(1)[0]
    # complete_movie = complete_movie_data.filter(lambda line: line!=complete_movie_header).map(lambda line:line.split(",")).\
    # map(lambda fields:(int(fields[0]), fields[1], fields[2])).cache()
    # complete_title = complete_movie.map(lambda x:(int(x[0]),x[1]))

    # print "There are %s movies in the complete dataset" % complete_title.count()

    movie_ID_with_ratings_RDD = (rating.map(lambda x: (x[1], x[2])).groupByKey())
    print "movie_ID_with_rating",movie_ID_with_ratings_RDD.take(3)
    movie_ID_with_avg_ratings_RDD = movie_ID_with_ratings_RDD.map(get_counts_and_averages)
    movie_rating_counts_RDD = movie_ID_with_avg_ratings_RDD.map(lambda x: (x[0], x[1][0]))
    print "move_rating_counts", movie_rating_counts_RDD.take(3)

    #adding new users
    new_user_id = 0
    new_user_ratings = [
        (0,260,9), # Star Wars (1977)
        (0,1,8), # Toy Story (1995)
        (0,16,7), # Casino (1995)
        (0,25,8), # Leaving Las Vegas (1995)
        (0,32,9), # Twelve Monkeys (a.k.a. 12 Monkeys) (1995)
        (0,335,4), # Flintstones, The (1994)
        (0,379,3), # Timecop (1994)
        (0,296,7), # Pulp Fiction (1994)
        (0,858,10) , # Godfather, The (1972)
        (0,50,8)] # Usual Suspects, The (1995)
    new_user_ratings_RDD = sc.parallelize(new_user_ratings)
    complete_data_with_new_ratings_RDD = rating.union(new_user_ratings_RDD)
    t0 = time()
    new_ratings_model = ALS.train(complete_data_with_new_ratings_RDD, best_rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)
    tt = time() - t0

    print "New model trained in %s seconds" % round(tt,3)

    #get top recommendations
    new_user_ratings_ids = map(lambda x: x[1], new_user_ratings)
    new_user_unrated_movies_RDD = (movies.filter(lambda x: x[0] not in new_user_ratings_ids).map(lambda x: (new_user_id, x[0])))
    print new_user_unrated_movies_RDD.take(3)

    new_user_recommendations_RDD = new_ratings_model.predictAll(new_user_unrated_movies_RDD)
    new_user_recommendations_rating_RDD = new_user_recommendations_RDD.map(lambda x: (str(x.product), x.rating))
    print "new_user_recommendations_rating",new_user_recommendations_rating_RDD.take(3)


    complete_title = movies.map(lambda x:(x[0],x[1]))
    print "complete_title",complete_title.take(3)


    new_user_recommendations_rating_title_and_count_RDD = new_user_recommendations_rating_RDD.join(complete_title).join(movie_rating_counts_RDD)
    print new_user_recommendations_rating_title_and_count_RDD.take(3)
    new_user_recommendations_rating_title_and_count_RDD = new_user_recommendations_rating_title_and_count_RDD.map(lambda r: (r[1][0][1], r[1][0][0], r[1][1]))

    top_movies = new_user_recommendations_rating_title_and_count_RDD.filter(lambda r: r[2]>=25).takeOrdered(25, key=lambda x: -x[1])

    print 'TOP recommended movies (with more than 25 reviews):\n%s' % '\n'.join(map(str, top_movies))







if __name__ == '__main__':
    cProfile.run('main()')





