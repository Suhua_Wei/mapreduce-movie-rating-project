# README #

A Project on Movie Rating Recommendations

### What is this repository for? ###

* Learning Hadoop and MapReduce
* Scale up to larger data sets using Amazon's Elastic MapReduce service
* Comparing performance with multiprocessing version using 10 CPUs

### How do I get set up? ###

* See the descriptions on the top of each script. 

### Who do I talk to? ###

s.wei@vikes.csuohio.edu